#include <Wire.h>
#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID           "6E400001-B5A3-F393-E0A9-E50E24DCCA9E" // UART service UUID
#define CHARACTERISTIC_UUID_RX "6E400002-B5A3-F393-E0A9-E50E24DCCA9E"
#define CHARACTERISTIC_UUID_TX "6E400003-B5A3-F393-E0A9-E50E24DCCA9E"

BLEServer *pServer = NULL;
BLECharacteristic * pTxCharacteristic;
bool deviceConnected = false;
bool oldDeviceConnected = false;


long timer = 0;
byte transmition = 0;
int MPU6050_ADDR = 0, minCoup = 101, myMax = 0, myMin = 0, nZeroVal = 0, nbCoup = 0, dCoup = 0;
int  listeSensor[10];
int16_t rawAccX, rawAccY, rawAccZ, rawTemp,rawGyroX, rawGyroY, rawGyroZ;
int16_t yAxis, myAxis;

uint8_t txValue = 0;

class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};
class MyCallbacks: public BLECharacteristicCallbacks {
    void onWrite(BLECharacteristic *pCharacteristic) {
      std::string rxValue = pCharacteristic->getValue();

      if (rxValue.length() > 0) {
        Serial.println("*********");
        Serial.print("Received Value: ");
        for (int i = 0; i < rxValue.length(); i++)
          Serial.print(rxValue[i]);

        Serial.println();
        Serial.println("*********");
      }
    }
};


void setup() {
 
  Serial.begin(115200);
  Wire.begin();
  for(int i = 0; i<128; i++){
    Wire.beginTransmission(i%128);
    transmition = Wire.endTransmission();
     if (transmition == 0) {
       MPU6050_ADDR = i;
       break;
     }
  }
  resetSensor();
  myWrite(0x6b, 0x00);

  
  // Create the BLE Device
  BLEDevice::init("MyPunchESP32");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pTxCharacteristic = pService->createCharacteristic(
                    CHARACTERISTIC_UUID_TX,
                    BLECharacteristic::PROPERTY_NOTIFY
                  );
                      
  pTxCharacteristic->addDescriptor(new BLE2902());

  BLECharacteristic * pRxCharacteristic = pService->createCharacteristic(
                       CHARACTERISTIC_UUID_RX,
                      BLECharacteristic::PROPERTY_WRITE
                    );
  pRxCharacteristic->setCallbacks(new MyCallbacks());
  // Start the service
  pService->start();

  // Start advertising
  pServer->getAdvertising()->start();
  Serial.println("Waiting a client connection to notify...");
}
void loop() {
  
    if (deviceConnected) {
        update();
        myZ();
        // bluetooth stack will go into congestion, if too many packets are sent
  }

    // disconnecting
    if (!deviceConnected && oldDeviceConnected) {
        delay(500); // give the bluetooth stack the chance to get things ready
        pServer->startAdvertising(); // restart advertising
        Serial.println("start advertising");
        oldDeviceConnected = deviceConnected;
    }
    // connecting
    if (deviceConnected && !oldDeviceConnected) {
    // do stuff here on connecting
        oldDeviceConnected = deviceConnected;
    }

}
void update() {
  
  Wire.beginTransmission(MPU6050_ADDR);
  Wire.write(0x3B);
  Wire.endTransmission(false);
  Wire.requestFrom((int)MPU6050_ADDR, 14, (int)true);
    
  rawAccX = Wire.read() << 8 | Wire.read();
  rawAccY = Wire.read() << 8 | Wire.read();
  rawAccZ = Wire.read() << 8 | Wire.read();
  rawTemp = Wire.read() << 8 | Wire.read();
  rawGyroX = Wire.read() << 8 | Wire.read();
  rawGyroY = Wire.read() << 8 | Wire.read();
  rawGyroZ = Wire.read() << 8 | Wire.read();
  
  delay(100);
}
void myZ() {
   int power = rawGyroX/93;
  power = sqrt(power * power);
  yAxis = sqrt(rawAccY/1038 * rawAccY/1038);
  
  if(rawAccY/16384.0 > .80 || rawAccY/16384.0 < -0.8) {
    dCoup +=1;
    if (dCoup > 5){
      power = 0;
      //Serial.println("AZEAZEAZEAZEAZEAZEAZE");
      nZeroVal = 0;
      myMin = 0;
      myMax = 0;
    }
  }
  if(power > myMax && power > minCoup && yAxis > 15) {
      myMax = power;
      myAxis = yAxis;
      nZeroVal = 0;
      dCoup = 0;
  } else if (power < myMin && power < (-minCoup)){
    myMin = power;
    dCoup = 0;
  } else if (power > (- minCoup) && power < minCoup) {
    nZeroVal += 1;
    dCoup = 0;
  }
  if (nZeroVal == 2) {
    nZeroVal = 0;
    if (myMax != 0) {
      nbCoup += 1;
             //Send here the value of the power replace 00 by the real value
      txValue = myMax - 100 - 31 + myAxis;
      pTxCharacteristic->setValue(&txValue, 1);
      pTxCharacteristic->notify();
      Serial.println(txValue);
      myMin = 0;
      myMax = 0;
      myAxis = 0;
    }
  }
   //Serial.println(power);
}

void myWrite (byte reg, byte dat) {
  Wire.beginTransmission(MPU6050_ADDR);
  Wire.write(reg);
  Wire.write(dat);
  Wire.endTransmission();
}
void resetSensor() {
  nZeroVal = 0;
  for(int i = 0; i < 10; i++) {
    listeSensor[i] = 0;
  }
}

